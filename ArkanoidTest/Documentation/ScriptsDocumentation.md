# Arkanoid

###class UI 
-Work with UI text. 

###class Platform
-Player behavior, movement restriction in the playing field.

###class Ball
-The behavior of the ball, the beginning of the movement (Fire1) acceleration (Fire2), just so faster) (well, the maximum acceleration value is 100).

###class BallVisible
-Visually rotates the ball on the motion vector

###Use Info (vectors) : http://mathus.ru/phys/vectors.pdf

Visualization of getting a direction vector when a ball collides with a player
![](Images/BallDirection.png)