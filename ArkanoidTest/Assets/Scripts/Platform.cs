﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Platform : MonoBehaviour
{
    public float PlatformSpeed = 0.1f;
 
    private float GetPlatformSpeed
    {
        get
        {
           return PlatformSpeed;
        }
        
        set 
        {
            if (PlatformSpeed > 0f)
            {
                PlatformSpeed = value;
            }
                Debug.Log("Value a speed of ball incorrect (need natural value)");
        }
    }
    
    private Vector3 playerPos = new Vector3(0, 0.5f, -7);


    // Update is called once per frame
    void Update()
    {
        float xPositiot = transform.position.x + (Input.GetAxis("Mouse X") * GetPlatformSpeed);
        playerPos = new Vector3(Mathf.Clamp(xPositiot,-7.5f,7.5f),0.5f,-7f);
        transform.position = playerPos;

    }
    
}